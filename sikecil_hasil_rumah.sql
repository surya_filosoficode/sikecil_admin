-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 03 Jul 2019 pada 19.46
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sikecil`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin`(`nama` VARCHAR(64), `email` TEXT, `pass` VARCHAR(64), `time_update` DATETIME, `id_admin_in` VARCHAR(12), `username` VARCHAR(64)) RETURNS varchar(14) CHARSET latin1
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
   
  select count(*) into count_row_user from admin where substr(id_admin,3,6) = left(NOW()+0, 6);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, email, username,  pass, '0', nama,  '0', id_admin_in, '0000-00-00 00:00:00');
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_hutang`(`id_tipe` INT, `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kontak`(`id_user_in` VARCHAR(17), `id_tipe_vdr` VARCHAR(64), `nama_vdr` TEXT, `email_vdr` TEXT, `tlp_vdr` VARCHAR(13), `alamat_ktr_vdr` TEXT, `alamat_krm_vdr` TEXT, `website` TEXT, `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from kontak where substr(id_vdr,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_vdr into last_key_user from kontak
  	where substr(id_vdr,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_vdr desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into kontak values(fix_key_user, id_user_in, id_tipe_vdr,  nama_vdr, email_vdr, tlp_vdr, alamat_ktr_vdr, alamat_krm_vdr, website, "0", time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pembelian`(`id_user_in` VARCHAR(17), `id_vendor` VARCHAR(28), `jenis_bayar` ENUM('0','1'), `tgl_tempo_start` DATE, `tgl_tempo_finish` DATE, `sisa_pinjaman` VARCHAR(50), `sts_piutang` ENUM('0','1'), `tipe_bayar` VARCHAR(50), `keterangan_bayar` TEXT, `deskripsi` TEXT, `no_faktur` TEXT, `tgl_pembelian` DATE, `id_produk` VARCHAR(28), `jml_produk` INT(11), `disc` INT(3), `total_bayar` VARCHAR(50), `time_update` DATETIME, `harga_satuan` VARCHAR(64)) RETURNS varchar(30) CHARSET latin1
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
   
  select count(*) into count_row_user from pembelian where substr(id_pembelian,19,8) = left(NOW()+0, 8) and id_user = id_user_in;
        
  select id_pembelian into last_key_user from pembelian
  	where substr(id_pembelian,19,8) = left(NOW()+0, 8)
    and id_user = id_user_in
  	order by id_pembelian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 8),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 12)+1);
      
  END IF;
  
  
  insert into pembelian values(fix_key_user, id_user_in, id_vendor,  jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_piutang, tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_pembelian, id_produk, jml_produk, disc, harga_satuan, total_bayar, "0", "0", time_update);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pengeluaran`(`id_user_in` VARCHAR(17), `id_vendor` VARCHAR(28), `jenis_bayar` ENUM('0','1'), `tgl_tempo_start` DATE, `tgl_tempo_finish` DATE, `sisa_pinjaman` VARCHAR(50), `sts_pinjaman` ENUM('0','1'), `tipe_bayar` TEXT, `keterangan_bayar` TEXT, `deskripsi` TEXT, `no_faktur` TEXT, `tgl_pengeluaran` DATETIME, `id_biaya` TEXT, `keterangan_biaya` TEXT, `total_biaya` VARCHAR(50), `time_update` DATETIME) RETURNS varchar(30) CHARSET latin1
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
   
  select count(*) into count_row_user from pengeluaran where substr(id_pengeluaran,19,8) = left(NOW()+0, 8) and id_user = id_user_in;
        
  select id_pengeluaran into last_key_user from pengeluaran
  	where substr(id_pengeluaran,19,8) = left(NOW()+0, 8)
    and id_user = id_user_in
  	order by id_pengeluaran desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 8),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 12)+1);
      
  END IF;
  
  insert into pengeluaran values(fix_key_user, id_user_in, id_vendor,  jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_pinjaman, tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_pengeluaran, id_biaya, keterangan_biaya, total_biaya, "0", "0", time_update);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_penjualan`(`id_user_in` VARCHAR(17), `id_vendor` VARCHAR(28), `jenis_bayar` ENUM('0','1'), `tgl_tempo_start` DATE, `tgl_tempo_finish` DATE, `sisa_pinjaman` VARCHAR(50), `sts_piutang` ENUM('0','1'), `tipe_bayar` VARCHAR(50), `keterangan_bayar` TEXT, `deskripsi` TEXT, `no_faktur` TEXT, `tgl_penjualan` DATE, `id_produk` VARCHAR(28), `jml_produk` INT(11), `disc` INT(3), `total_bayar` VARCHAR(50), `time_update` DATETIME, `harga_satuan` VARCHAR(64)) RETURNS varchar(30) CHARSET latin1
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
   
  select count(*) into count_row_user from penjualan where substr(id_penjualan,19,8) = left(NOW()+0, 8) and id_user = id_user_in;
        
  select id_penjualan into last_key_user from penjualan
  	where substr(id_penjualan,19,8) = left(NOW()+0, 8)
    and id_user = id_user_in
  	order by id_penjualan desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 8),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 12)+1);
      
  END IF;
  
  
	insert into penjualan values(fix_key_user, id_user_in, id_vendor,  jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_piutang, tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_penjualan, id_produk, jml_produk, disc, harga_satuan, total_bayar, "0", "0", time_update);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_piutang`(`id_tipe` INT, `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_produk`(`id_tipe` VARCHAR(64), `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `harga_beli_satuan` VARCHAR(64), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, harga_beli_satuan, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_user`(`username` VARCHAR(64), `email` TEXT, `password` VARCHAR(64), `nama_com` VARCHAR(64), `alamat_com` TEXT, `time_update` DATETIME, `id_admin` VARCHAR(12), `tlp_com` VARCHAR(13), `sts_active` ENUM('0','1','2'), `nama_user` TEXT) RETURNS varchar(17) CHARSET latin1
    NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(17);
  
  select count(*) into count_row_user from user 
  	where substr(id_user,4,8) = left(NOW()+0, 8);
        
  select id_user into last_key_user from user
  	where substr(id_user,4,8) = left(NOW()+0, 8)
  	order by id_user desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("USR",left(NOW()+0, 8),"000001");
  else
      	set fix_key_user = concat("USR", right(last_key_user, 14)+1);
      
  END IF;
  
  insert into user values(fix_key_user, username, email, nama_user, password, nama_com, alamat_com, tlp_com, sts_active, "0", time_update, id_admin);
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `username`, `password`, `status_active`, `nama`, `is_delete`, `admin_del`, `time_update`) VALUES
('AD2019050001', 'suryahanggarass@gmail.com', 'suryahanggara', '21232f297a57a5a743894a0e4a801fc3', '1', 'surya hanggaras', '0', '0', '2019-05-05 00:00:00'),
('AD2019050002', 'donisiregar@gmail.com', 'donisiregar', '21232f297a57a5a743894a0e4a801fc3', '0', 'doni siregar', '0', '0', '0000-00-00 00:00:00'),
('AD2019050003', 'roberthanggara19@gmail.com', 'suryahanggaras', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya', '1', 'qV/9yPjTM9Dd', '2019-05-08 10:50:08'),
('AD2019050004', 'suryahanggasra@gmail.com', 'suryadi', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya', '1', 'qV/9yPjTM9Dd', '2019-05-08 10:50:03'),
('AD2019050005', 'suryahanggaras@gmail.com', 'suryahanggarass', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya hanggara', '1', 'YVuqb09LxHU9', '2019-05-08 10:49:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak`
--

CREATE TABLE IF NOT EXISTS `kontak` (
  `id_vdr` varchar(28) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_tipe_vdr` varchar(64) NOT NULL,
  `nama_vdr` text NOT NULL,
  `email_vdr` text NOT NULL,
  `tlp_vdr` varchar(13) NOT NULL,
  `alamat_ktr_vdr` text NOT NULL,
  `alamat_krm_vdr` text NOT NULL,
  `website` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_vdr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kontak`
--

INSERT INTO `kontak` (`id_vdr`, `id_user`, `id_tipe_vdr`, `nama_vdr`, `email_vdr`, `tlp_vdr`, `alamat_ktr_vdr`, `alamat_krm_vdr`, `website`, `is_delete`, `time_update`, `id_admin`) VALUES
('USR20190512000011-2019060002', 'USR20190623000019', '1', 'surya', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'google', '0', '0000-00-00 00:00:00', '1'),
('USR20190512000011-2019060003', 'USR20190623000019', '1', 'filcod', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'google', '0', '2019-06-24 04:03:14', '0'),
('USR20190512000011-2019060005', 'USR20190623000019', '1', 'suryafilosoficode', 'suryafilosoficode@gmail.com', '081230658447', 'Jogjakarta', 'Jogjakarta', 'google', '0', '2019-06-24 04:05:47', '0'),
('USR20190512000011-2019070001', 'USR20190512000011', '1', 'filcod', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'https://www.youtube.com/', '0', '2019-07-01 04:17:39', '0'),
('USR20190512000012-2019060001', 'USR20190512000012', '1', 'surya', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'google', '0', '0000-00-00 00:00:00', '1'),
('USR20190512000015-2019060001', 'USR20190512000015', '1', 'surya', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'google', '0', '0000-00-00 00:00:00', '1'),
('USR20190623000019-2019070001', 'USR20190623000019', 'suplier', 'PT Nusa persada merdeka', 'suryahanggara@gmail.com', '08123657412', 'malang', 'malang', 'google.com', '0', '2019-07-01 04:21:56', '0'),
('USR20190623000019-2019070002', 'USR20190623000019', 'suplier', 'PT Telekomunikasi indonesia', 'suryahanggara@gmail.com', '111222333666', 'malang', 'malang', 'googlq.com', '0', '2019-07-01 04:22:54', '0'),
('USR20190623000019-2019070003', 'USR20190623000019', 'vendor', 'surya hanggara', 'asuryahanggara@gmail.com', '0812306957741', 'malangs', 'malangs', 'googles', '0', '2019-07-01 04:54:11', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak_tipe`
--

CREATE TABLE IF NOT EXISTS `kontak_tipe` (
  `id_tipe_vdr` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tipe_vdr` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_tipe_vdr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `kontak_tipe`
--

INSERT INTO `kontak_tipe` (`id_tipe_vdr`, `nama_tipe_vdr`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'suplier', '0', '0000-00-00 00:00:00', '0'),
(2, 'vendor', '0', '2019-05-05 00:00:00', '0'),
(3, 'dimasxxx xasdas23432 sdsadas-gfsfasds', '1', '2019-07-03 06:38:35', 'AD2019050001'),
(4, 'dono', '1', '2019-07-03 06:38:40', 'AD2019050001'),
(5, 'johan aa', '1', '2019-07-03 06:52:14', 'AD2019050001'),
(6, 'nomadenff dsadsad', '1', '2019-07-03 06:52:17', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE IF NOT EXISTS `pembelian` (
  `id_pembelian` varchar(30) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vdr` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `sisa_pinjaman` varchar(50) NOT NULL,
  `sts_pinjaman` enum('0','1') NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_pembelian` datetime NOT NULL,
  `id_prd` varchar(28) NOT NULL,
  `jml_produk` int(11) NOT NULL,
  `disc` int(3) NOT NULL,
  `harga_satuan` varchar(64) NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_pembelian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `id_user`, `id_vdr`, `jenis_bayar`, `tgl_tempo_start`, `tgl_tempo_finish`, `sisa_pinjaman`, `sts_pinjaman`, `tipe_bayar`, `keterangan_bayar`, `deskripsi`, `no_faktur`, `tgl_pembelian`, `id_prd`, `jml_produk`, `disc`, `harga_satuan`, `total_bayar`, `sts_active`, `is_delete`, `time_update`) VALUES
('USR20190623000018-201907020001', 'USR20190623000018', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '1', 'bank', 'bank BRI', 'pembayaran pembelian pisang keju', 'xxxxds', '2019-07-03 00:00:00', 'USR20190623000019-2019060006', 20, 5, '', '20000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907020001', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '1', 'bank', 'bank BRI', 'pembayaran pembelian pisang keju', 'xxxxds', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 20, 5, '', '20000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907020002', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '1', 'bank', 'bank BRI', 'pembayaran pembelian pisang keju', 'xxxxds', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 20, 5, '', '20000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907030001', 'USR20190623000019', '', '0', '0000-00-00', '0000-00-00', '0', '0', 'Cash', 'Pembayran Cash', 'Pembayaran tanah pakis', '123', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 0, 5, '20000', '4000000', '0', '0', '2019-07-03 05:12:31'),
('USR20190623000019-201907030002', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'Cash', 'Pembayran Cash', 'Pembayaran tanah pakis', '123', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 0, 5, '20000', '4000000', '0', '0', '2019-07-03 05:12:55'),
('USR20190623000019-201907030003', 'USR20190623000019', 'USR20190623000019-2019070003', '1', '2019-07-03', '2019-07-29', '4000000', '0', 'Cash', 'Pembayran Cash', 'Pembayaran tanah pakis malang', '123xx', '2019-07-03 00:00:00', 'USR20190623000019-2019060006', 20, 10, '70000', '1260000', '0', '0', '2019-07-03 05:34:09'),
('USR20190623000019-201907030004', 'USR20190623000019', 'USR20190623000019-2019070003', '1', '2019-07-03', '2019-07-29', '4000000', '0', 'Cash', 'Pembayran Cash', 'Pembayaran tanah pakis malang', '123xx', '2019-07-03 00:00:00', 'USR20190623000019-2019060006', 20, 10, '50000', '900000', '0', '0', '2019-07-03 05:34:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengeluaran`
--

CREATE TABLE IF NOT EXISTS `pengeluaran` (
  `id_pengeluaran` varchar(30) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vdr` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `sisa_pinjaman` varchar(50) NOT NULL,
  `sts_pinjaman` enum('0','1') NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_pengeluaran` datetime NOT NULL,
  `id_biaya` varchar(28) NOT NULL,
  `keterangan_biaya` text NOT NULL,
  `total_biaya` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_pengeluaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengeluaran`
--

INSERT INTO `pengeluaran` (`id_pengeluaran`, `id_user`, `id_vdr`, `jenis_bayar`, `tgl_tempo_start`, `tgl_tempo_finish`, `sisa_pinjaman`, `sts_pinjaman`, `tipe_bayar`, `keterangan_bayar`, `deskripsi`, `no_faktur`, `tgl_pengeluaran`, `id_biaya`, `keterangan_biaya`, `total_biaya`, `sts_active`, `is_delete`, `time_update`) VALUES
('USR20190623000019-201907020001', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'cash', 'bayar menggunakan bank BCA', 'kosongan', 'ttyyff', '0000-00-00 00:00:00', 'Biaya Telephone', 'Pembayaran Telephon Bulanan', '10000000', '1', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907030001', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'cash c', 'BRI c', 'Pembayaran Biaya PDAM c', '1122332', '2019-07-04 00:00:00', 'biaya pdams', 'Pembayaran biaya PDAMs', '600000', '0', '0', '2019-07-03 10:34:01'),
('USR20190623000019-201907030002', 'USR20190623000019', 'USR20190623000019-2019070003', '1', '2019-07-03', '2019-07-30', '700000', '0', '0', 'hutang', 'Pembelian sapu lidi hutang', '112233', '2019-07-03 00:00:00', 'biaya pdam', 'Pembayaran biaya PDAM', '500000', '0', '0', '2019-07-03 10:34:30'),
('USR20190623000019-201907030003', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', '1', 'cash', 'Melalui Bank BRI', '112233', '2019-07-03 00:00:00', 'biaya pdam', 'Pembayaran biaya PDAM', '500000', '0', '0', '2019-07-03 10:35:25'),
('USR20190623000019-201907030004', 'USR20190623000019', 'USR20190623000019-2019070003', '1', '2019-07-03', '2019-07-29', '500000', '0', 'cash', 'BRI', 'Pembayaran Biaya PDAM', '112233', '2019-07-03 00:00:00', 'biaya pdam', 'Pembayaran biaya PDAM', '500000', '0', '0', '2019-07-03 10:37:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `id_penjualan` varchar(30) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vdr` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `sisa_pinjaman` varchar(50) NOT NULL,
  `sts_pinjaman` enum('0','1') NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_penjualan` datetime NOT NULL,
  `id_prd` varchar(28) NOT NULL,
  `jml_produk` int(11) NOT NULL,
  `disc` int(3) NOT NULL,
  `harga_satuan` varchar(64) NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_penjualan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan`
--

INSERT INTO `penjualan` (`id_penjualan`, `id_user`, `id_vdr`, `jenis_bayar`, `tgl_tempo_start`, `tgl_tempo_finish`, `sisa_pinjaman`, `sts_pinjaman`, `tipe_bayar`, `keterangan_bayar`, `deskripsi`, `no_faktur`, `tgl_penjualan`, `id_prd`, `jml_produk`, `disc`, `harga_satuan`, `total_bayar`, `sts_active`, `is_delete`, `time_update`) VALUES
('USR20190623000018-201907020001', 'USR20190623000018', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'bank', 'bayar cetak menggunakan BRI', 'pembelian usaha', 'xxxx', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 20, 10, '', '100000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907020001', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'bank', 'bayar cetak menggunakan BRI', 'pembelian usaha', 'xxxx', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 20, 10, '', '100000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907020002', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'bank', 'bayar cetak menggunakan BRI', 'pembelian usaha', 'xxxx', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 20, 10, '', '100000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907020003', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'bank', 'bayar cetak menggunakan BRI', 'pembelian usaha', 'xxxx', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 20, 10, '', '100000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907020004', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'bank', 'bayar cetak menggunakan BRI', 'pembelian usaha', 'xxxx', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 20, 10, '', '100000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907020005', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', 'bank', 'bayar cetak menggunakan BRI', 'pembelian usaha', 'xxxx', '0000-00-00 00:00:00', 'USR20190623000019-2019060006', 20, 10, '', '100000000', '0', '0', '0000-00-00 00:00:00'),
('USR20190623000019-201907030001', 'USR20190623000019', 'USR20190623000019-2019070003', '1', '2019-07-03', '2019-07-29', '400000', '0', '0', 'hutang', 'Pembelian sapu lidi hutang', '11231s', '2019-07-03 00:00:00', 'USR20190623000019-2019060008', 0, 5, '20000', '380000', '0', '0', '2019-07-03 09:24:45'),
('USR20190623000019-201907030002', 'USR20190623000019', 'USR20190623000019-2019070003', '0', '0000-00-00', '0000-00-00', '0', '0', '1', 'Cash', 'Pembelian sapu lidi hutang', '112233', '2019-07-03 00:00:00', 'USR20190623000019-2019060008', 25, 5, '20000', '500000', '0', '0', '2019-07-03 09:25:24'),
('USR20190623000019-201907030003', 'USR20190623000019', 'USR20190623000019-2019070003', '1', '2019-07-03', '2019-07-30', '700000', '0', '0', 'hutang', 'Pembelian sapu lidi hutang', '112233', '2019-07-03 00:00:00', 'USR20190623000019-2019060008', 25, 5, '30000', '712500', '0', '0', '2019-07-03 09:38:33'),
('USR20190623000019-201907030004', 'USR20190623000019', 'USR20190623000019-2019070003', '1', '2019-07-03', '2019-07-30', '500000', '0', '0', 'hutang', 'Pembelian sapu lidi hutang', '112233', '2019-07-03 00:00:00', 'USR20190623000019-2019060008', 0, 5, '20000', '475000', '0', '0', '2019-07-03 09:40:26'),
('USR20190623000019-201907030005', 'USR20190623000019', 'USR20190623000019-2019070003', '1', '2019-07-03', '2019-07-30', '500000', '0', '0', 'hutang', 'Pembelian sapu lidi hutang', '112233', '2019-07-03 00:00:00', 'USR20190623000019-2019060008', 0, 5, '20000', '475000', '0', '0', '2019-07-03 09:40:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `id_prd` varchar(28) NOT NULL,
  `id_tipe` varchar(64) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `satuan_prd` varchar(32) NOT NULL,
  `harga_prd` varchar(32) NOT NULL,
  `harga_beli_satuan` varchar(64) NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_opname` int(11) NOT NULL,
  `nama_prd` varchar(64) NOT NULL,
  `desk_prd` text NOT NULL,
  `sts_jual` enum('0','1') NOT NULL,
  `sts_beli` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_prd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id_prd`, `id_tipe`, `id_user`, `satuan_prd`, `harga_prd`, `harga_beli_satuan`, `stok`, `stok_opname`, `nama_prd`, `desk_prd`, `sts_jual`, `sts_beli`, `is_delete`, `time_update`, `id_admin`) VALUES
('USR20190623000019-2019060006', 'Barang', 'USR20190623000019', 'pcs', '1000', '', 10, 0, 'donat', 'donat', '0', '0', '0', '2019-06-29 04:06:15', '0'),
('USR20190623000019-2019060007', 'okokok', 'USR20190623000019', 'pcs', '5000', '', 20, 0, 'kaos', 'oblong', '1', '0', '0', '2019-06-29 04:09:47', '0'),
('USR20190623000019-2019060008', 'Barang', 'USR20190623000019', 'jam apa', '4200000', '', 10, 0, 'Pijat enak pol', 'Pijat enak pol', '1', '1', '0', '2019-06-29 04:15:09', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_tipe`
--

CREATE TABLE IF NOT EXISTS `produk_tipe` (
  `id_tipe` int(11) NOT NULL AUTO_INCREMENT,
  `ket_tipe` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_tipe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `produk_tipe`
--

INSERT INTO `produk_tipe` (`id_tipe`, `ket_tipe`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'Barang', '0', '2019-05-13 06:28:55', 'AD2019050001'),
(2, 'Jasa', '0', '2019-05-13 06:29:40', 'AD2019050001'),
(3, 'masis', '1', '2019-07-03 06:51:17', 'AD2019050001'),
(4, 'simad', '1', '2019-07-03 06:51:21', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(11) NOT NULL AUTO_INCREMENT,
  `ket_satuan` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_satuan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `ket_satuan`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'cm', '0', '2019-07-03 07:05:57', 'AD2019050001'),
(2, 'jam', '0', '2019-07-03 07:07:21', 'AD2019050001'),
(3, 'menit', '0', '2019-07-03 07:07:27', 'AD2019050001'),
(4, 'detik', '0', '2019-07-03 07:07:34', 'AD2019050001'),
(5, 'asu kon', '1', '2019-07-03 07:08:16', 'AD2019050001'),
(6, 'temenan aa', '1', '2019-07-03 07:08:11', 'AD2019050001'),
(7, 'wih kon', '1', '2019-07-03 07:09:15', 'AD2019050001'),
(8, 'jamban jembarsut', '1', '2019-07-03 07:09:12', 'AD2019050001'),
(9, 'dafuk donat', '1', '2019-07-03 07:09:09', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_bayar`
--

CREATE TABLE IF NOT EXISTS `tipe_bayar` (
  `id_bayar` int(11) NOT NULL AUTO_INCREMENT,
  `cara_bayar` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_bayar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `tipe_bayar`
--

INSERT INTO `tipe_bayar` (`id_bayar`, `cara_bayar`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'Lainnya', '0', '2019-07-03 00:00:00', '0'),
(2, 'Cash', '0', '2019-07-03 00:00:00', '0'),
(3, 'Transfer Bank', '0', '2019-07-03 00:00:00', '0'),
(4, 'Wesel', '0', '2019-07-03 00:00:00', '0'),
(5, 'Cek Langsung', '0', '2019-07-03 00:00:00', '0'),
(6, 'caraku asd asd as', '1', '2019-07-03 06:53:23', 'AD2019050001'),
(7, 'caramuasdasda s sad', '1', '2019-07-03 06:53:18', 'AD2019050001'),
(8, 'asru ss sadsa dsa fasdsa', '1', '2019-07-03 06:53:14', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_biaya`
--

CREATE TABLE IF NOT EXISTS `tipe_biaya` (
  `id_biaya` int(11) NOT NULL AUTO_INCREMENT,
  `ket_tipe_biaya` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_biaya`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `tipe_biaya`
--

INSERT INTO `tipe_biaya` (`id_biaya`, `ket_tipe_biaya`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'Biaya admin', '0', '0000-00-00 00:00:00', '0'),
(2, 'Biaya listrik', '0', '2019-07-03 00:00:00', '0'),
(3, 'Biaya Balik Nama xx', '1', '2019-07-03 06:58:13', 'AD2019050001'),
(4, 'Balik Badan zz', '1', '2019-07-03 06:58:08', 'AD2019050001'),
(5, 'Testing broo', '1', '2019-07-03 06:58:38', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` varchar(17) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` text NOT NULL,
  `nama_user` text NOT NULL,
  `password` varchar(64) NOT NULL,
  `nama_com` text NOT NULL,
  `alamat_com` text NOT NULL,
  `tlp_com` varchar(13) NOT NULL,
  `sts_active` enum('0','1','2') NOT NULL,
  `sts_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `nama_user`, `password`, `nama_com`, `alamat_com`, `tlp_com`, `sts_active`, `sts_delete`, `time_update`, `id_admin`) VALUES
('USR20190512000011', 'suryahanggara', 'suryahanggarao@gmail.com', 'suryarobertson', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'filososfi kopi', 'surabaya', '081230695774', '1', '0', '2019-05-12 02:46:39', '0'),
('USR20190620000012', 'wahyu', 'robert@gmail.com', 'surya hanggara', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'kanca_lawas', 'malang', '081230695774', '0', '0', '2019-06-20 00:00:00', '0'),
('USR20190620000013', 'dimas', 'dimas@gmail.com', 'dimas', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'dimas com', 'malang', '0071203218', '0', '0', '0000-00-00 00:00:00', '0'),
('USR20190620000014', 'dimas', 'dimas@gmail.com', 'dimas', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'dimas com', 'malang', '0071203218', '0', '0', '0000-00-00 00:00:00', '0'),
('USR20190620000015', 'dimas', 'dimas@gmail.com', 'dimas', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'dimas com', 'malang', '0071203218', '0', '0', '0000-00-00 00:00:00', '0'),
('USR20190620000016', 'suryahanggaraz', 'suryahanggaraz@gmail.com', 'surya hanggara', 'ea1b9d779a37fa378d87c40dd6a56fcd491a7c9bef3a1f6e40228031bf00ac68', 'surya hanggara', 'malang', '081230695774', '0', '0', '2019-06-20 08:46:40', '0'),
('USR20190623000017', 'surya', 'suryafilosoficode@gmail.com', 'surya hanggara', 'b1f42dffc2f1b7577ba304fe7d1a6f28c9440d129147c00370829db54cc132e1', 'filosofi_code', 'malang', '081230695774', '1', '0', '2019-06-23 08:56:21', '0'),
('USR20190623000018', 'suryah', 'roberthanggara19@gmail.com', 'surya hanggara', 'ea1b9d779a37fa378d87c40dd6a56fcd491a7c9bef3a1f6e40228031bf00ac68', 'filosofi_code', 'malang', '081230695774', '1', '0', '2019-06-23 05:20:26', '0'),
('USR20190623000019', 'suryax', 'suryahanggara@gmail.com', 'surya hanggara', '671b2e039185e0baf32003ad023bab8a32e415fe94960b8871fea1248fce4370', 'filcode', 'malang', '081230695774', '1', '0', '2019-06-23 06:01:49', '0');

--
-- Trigger `user`
--
DROP TRIGGER IF EXISTS `auto_id_user`;
DELIMITER //
CREATE TRIGGER `auto_id_user` BEFORE INSERT ON `user`
 FOR EACH ROW BEGIN
  INSERT INTO user_seq VALUES (NULL,left(NOW()+0, 8));
  SET NEW.id_user = CONCAT('USR', left(NOW()+0, 8), LPAD(LAST_INSERT_ID(), 6, '0'));
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_seq`
--

CREATE TABLE IF NOT EXISTS `user_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data untuk tabel `user_seq`
--

INSERT INTO `user_seq` (`id`, `tgl`) VALUES
(1, '20190509'),
(2, '20190512'),
(3, '20190512'),
(4, '20190512'),
(5, '20190512'),
(6, '20190512'),
(7, '20190512'),
(8, '20190512'),
(9, '20190512'),
(10, '20190512'),
(11, '20190512'),
(12, '20190620'),
(13, '20190620'),
(14, '20190620'),
(15, '20190620'),
(16, '20190620'),
(17, '20190623'),
(18, '20190623'),
(19, '20190623');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_vert`
--

CREATE TABLE IF NOT EXISTS `user_vert` (
  `id_user` varchar(17) NOT NULL,
  `time` datetime NOT NULL,
  `param` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_vert`
--

INSERT INTO `user_vert` (`id_user`, `time`, `param`, `code`) VALUES
('USR20190620000016', '2019-06-20 08:46:40', '9bfd608db3ac9b9c56908d935263f3e4', 'c4d447f6c8b5b0740b54804ae5d9cee4'),
('USR20190512000011', '2019-06-23 17:59:07', '5a75a99fe1eb4d8bb6633643905f11e4', 'fa1aa38f73851ddb50f3396fa064c157'),
('USR20190620000012', '2019-06-24 17:01:48', '7b76a18f2be597d02cfd1fa0a4eff2d0', 'dfae5658701f71bd7996c76d19ceeeb7');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_vert_ch_pass`
--

CREATE TABLE IF NOT EXISTS `user_vert_ch_pass` (
  `id_user` varchar(17) NOT NULL,
  `time` datetime NOT NULL,
  `param` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_vert_ch_pass`
--

INSERT INTO `user_vert_ch_pass` (`id_user`, `time`, `param`, `code`) VALUES
('USR20190512000011', '2019-06-23 17:53:13', '0f5dfd552fe5a0b7c3f945444cdd898e', '3def6ca1fd712e5e07c00bd35b55b677'),
('USR20190623000019', '2019-06-24 14:50:33', 'c3acb1355134d99247b1d5763fa0168d', 'fe9673881b4062400437145306f59ad1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
