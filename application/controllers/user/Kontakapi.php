<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontakapi extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
        $this->load->model("user/kontak_main", "pm");

        $this->load->library("encrypt");
		
		$this->load->library("get_identity");
		$this->load->library("response_message");

	}

#=============================================================================#
#-------------------------------------------kontak_tipe----------------------#
#=============================================================================#
    public function val_get_kontak_tipe_api(){
        $config_val_input = array(
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_kontak_tipe_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_kontak_tipe_api()){
            $token = $this->input->post("token");
            if($token == "FC094X"){
                if($this->val_get_kontak_tipe_api()){
                    $data = $this->mm->get_data_all_where("kontak_tipe", array("is_delete"=>"0", "is_delete"=> "0"));
                    if($data){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        $msg_detail["item"] = $data;
                    }
                }else{
                    $msg_detail["token"] = strip_tags(form_error('token'));
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
    	print_r(json_encode($data));
    }
#=============================================================================#
#-------------------------------------------kontak_tipe----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------kontak_main----------------------#
#=============================================================================#
    public function val_form_kontak_user(){
        $config_val_input = array(
                array(
                    'field'=>'id_user',
                    'label'=>'id_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_tipe_vdr',
                    'label'=>'id_tipe_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_vdr',
                    'label'=>'nama_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'email_vdr',
                    'label'=>'email_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )     
                ),array(
                    'field'=>'tlp_vdr',
                    'label'=>'tlp_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'alamat_ktr_vdr',
                    'label'=>'alamat_ktr_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'alamat_krm_vdr',
                    'label'=>'alamat_krm_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'website',
                    'label'=>'website',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_kontak_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                    "id_tipe_vdr"=>"",
                    "nama_vdr"=>"",
                    "email_vdr"=>"",
                    "tlp_vdr"=>"",
                    "alamat_ktr_vdr"=>"",
                    "alamat_krm_vdr"=>"",
                    "website"=>"",
                    "token"=>""
                );

        if($this->val_form_kontak_user()){
            $id_user = $this->input->post("id_user");
            $id_tipe_vdr = $this->input->post("id_tipe_vdr");
            $nama_vdr = $this->input->post("nama_vdr");
            $email_vdr = $this->input->post("email_vdr");
            $tlp_vdr = $this->input->post("tlp_vdr");
            $alamat_ktr_vdr = $this->input->post("alamat_ktr_vdr");
            $alamat_krm_vdr = $this->input->post("alamat_krm_vdr");
            $website = $this->input->post("website");

            $token = $this->input->post("token");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $insert = $this->pm->insert_kontak($id_user, $id_tipe_vdr, $nama_vdr, $email_vdr, $tlp_vdr, $alamat_ktr_vdr, $alamat_krm_vdr, $website, $time_update, $id_admin);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail = array(
                            "id_user"   =>strip_tags(form_error('id_user')),
                            "id_tipe_vdr"   =>strip_tags(form_error('id_tipe_vdr')),
                            "nama_vdr"=>strip_tags(form_error('nama_vdr')),
                            "email_vdr" =>strip_tags(form_error('email_vdr')),
                            "tlp_vdr"      =>strip_tags(form_error('tlp_vdr')),
                            "alamat_ktr_vdr"  =>strip_tags(form_error('alamat_ktr_vdr')),
                            "alamat_krm_vdr"  =>strip_tags(form_error('alamat_krm_vdr')),
                            "website"  =>strip_tags(form_error('website')),
                            "token"     =>strip_tags(form_error('token'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_kontak_user(){
        $id     = $this->input->post("id_vdr");
        $data   = $this->pm->get_kontak(array("id_vdr"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

    public function update_kontak_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                    "id_tipe_vdr"=>"",
                    "nama_vdr"=>"",
                    "email_vdr"=>"",
                    "tlp_vdr"=>"",
                    "alamat_ktr_vdr"=>"",
                    "alamat_krm_vdr"=>"",
                    "website"=>"",
                    "token"=>""
                );

        if($this->val_form_kontak_user()){
            $id_user = $this->input->post("id_user");
            $id_tipe_vdr = $this->input->post("id_tipe_vdr");
            $nama_vdr = $this->input->post("nama_vdr");
            $email_vdr = $this->input->post("email_vdr");
            $tlp_vdr = $this->input->post("tlp_vdr");
            $alamat_ktr_vdr = $this->input->post("alamat_ktr_vdr");
            $alamat_krm_vdr = $this->input->post("alamat_krm_vdr");
            $website = $this->input->post("website");

            $token = $this->input->post("token");

            $id_vdr    = $this->input->post("id_vdr");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $set = array(
                        "id_tipe_vdr"=>$id_tipe_vdr,
                        "nama_vdr"=>$nama_vdr,
                        "email_vdr"=>$email_vdr,
                        "tlp_vdr"=>$tlp_vdr,
                        "alamat_ktr_vdr"=>$alamat_ktr_vdr,
                        "alamat_krm_vdr"=>$alamat_krm_vdr,
                        "website"=>$website
                    );

                $where = array(
                            "id_vdr"=>$id_vdr
                        );

                $update = $this->mm->update_data("kontak", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }else {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                }
            }
        }else{            
            $msg_detail = array(
                            "id_user"   =>strip_tags(form_error('id_user')),
                            "id_tipe_vdr"   =>strip_tags(form_error('id_tipe_vdr')),
                            "nama_vdr"=>strip_tags(form_error('nama_vdr')),
                            "email_vdr" =>strip_tags(form_error('email_vdr')),
                            "tlp_vdr"      =>strip_tags(form_error('tlp_vdr')),
                            "alamat_ktr_vdr"  =>strip_tags(form_error('alamat_ktr_vdr')),
                            "alamat_krm_vdr"  =>strip_tags(form_error('alamat_krm_vdr')),
                            "website"  =>strip_tags(form_error('website')),
                            "token"     =>strip_tags(form_error('token'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_kontak_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete()){
            $id_vdr     = $this->input->post("id_vdr");
            $token      = $this->input->post("token");
            if($token == "FC094X"){
                if($this->mm->delete_data("kontak", array("id_vdr"=>$id_vdr))){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                }
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


    public function val_get_kontak_api(){
        $config_val_input = array(
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_kontak_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_kontak_api()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                if($this->val_get_kontak_api()){
                    $data = $this->pm->get_kontak(array("p.id_user"=>$id_user, "p.is_delete"=>"0"));
                    if($data){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        $msg_detail["item"] = $data;
                    }
                }else{
                    $msg_detail["token"] = strip_tags(form_error('token'));
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        // print_r("<pre>");
        // print_r($data);
        print_r(json_encode($data));
    }
#=============================================================================#
#-------------------------------------------kontak_main----------------------#
#=============================================================================#


}
?>