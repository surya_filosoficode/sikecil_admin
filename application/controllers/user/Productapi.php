<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Productapi extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
        $this->load->model("user/product_main", "pm");

        $this->load->library("encrypt");
		
		$this->load->library("get_identity");
		$this->load->library("response_message");

	}

#=============================================================================#
#-------------------------------------------product_tipe----------------------#
#=============================================================================#
    public function val_get_product_tipe_api(){
        $config_val_input = array(
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_product_tipe_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_product_tipe_api()){
            $token = $this->input->post("token");
            if($token == "FC094X"){
                if($this->val_get_product_tipe_api()){
                    $data = $this->mm->get_data_all_where("produk_tipe", array("is_delete"=>"0", "is_delete"=> "0"));
                    if($data){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        $msg_detail["item"] = $data;
                    }
                }else{
                    $msg_detail["token"] = strip_tags(form_error('ket_tipe'));
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
    	print_r(json_encode($data));
    }
#=============================================================================#
#-------------------------------------------product_tipe----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------product_main----------------------#
#=============================================================================#
    public function val_form_product_user(){
        $config_val_input = array(
                array(
                    'field'=>'id_tipe',
                    'label'=>'id_tipe',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_user',
                    'label'=>'id_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'satuan_prd',
                    'label'=>'satuan_prd',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'harga_prd',
                    'label'=>'harga_prd',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )     
                ),array(
                    'field'=>'harga_beli_satuan',
                    'label'=>'harga_beli_satuan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )     
                ),array(
                    'field'=>'stok',
                    'label'=>'stok',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_prd',
                    'label'=>'nama_prd',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'desk_prd',
                    'label'=>'desk_prd',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'sts_jual',
                    'label'=>'sts_jual',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'sts_beli',
                    'label'=>'sts_beli',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_product_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_tipe"=>"",
                    "id_user"=>"",
                    "satuan_prd"=>"",
                    "harga_prd"=>"",
                    "harga_beli_satuan"=>"",
                    "stok"=>"",
                    "nama_prd"=>"",
                    "desk_prd"=>"",
                    "sts_jual"=>"",
                    "sts_beli"=>"",
                    "token"=>""
                );

        if($this->val_form_product_user()){
            $id_tipe            = $this->input->post("id_tipe");
            $id_user            = $this->input->post("id_user");
            $satuan_prd         = $this->input->post("satuan_prd");
            $harga_prd          = $this->input->post("harga_prd");
            $harga_beli_satuan  = $this->input->post("harga_beli_satuan");
            $stok               = $this->input->post("stok");
            $stok_opname        = "0";
            $nama_prd           = $this->input->post("nama_prd");
            $desk_prd           = $this->input->post("desk_prd");
            $sts_jual           = $this->input->post("sts_jual");
            $sts_beli           = $this->input->post("sts_beli");

            $token = $this->input->post("token");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $insert = $this->pm->insert_produk($id_tipe, $id_user, $satuan_prd, $harga_prd, $harga_beli_satuan, $stok, $stok_opname, $nama_prd, $desk_prd, $sts_jual, $sts_beli, $time_update, $id_admin);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail = array(
                            "id_tipe"   =>strip_tags(form_error('id_tipe')),
                            "id_user"   =>strip_tags(form_error('id_user')),
                            "satuan_prd"=>strip_tags(form_error('satuan_prd')),
                            "harga_prd" =>strip_tags(form_error('harga_prd')),
                            "stok"      =>strip_tags(form_error('stok')),
                            "nama_prd"  =>strip_tags(form_error('nama_prd')),
                            "desk_prd"  =>strip_tags(form_error('desk_prd')),
                            "sts_jual"  =>strip_tags(form_error('sts_jual')),
                            "sts_beli"  =>strip_tags(form_error('sts_beli')),
                            "token"     =>strip_tags(form_error('token'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_product_user(){
        $id     = $this->input->post("id_prd");
        $data   = $this->pm->get_produk(array("id_prd"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

    public function update_product_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_tipe"=>"",
                    "id_user"=>"",
                    "satuan_prd"=>"",
                    "harga_prd"=>"",
                    "harga_beli_satuan"=>"",
                    "stok"=>"",
                    "nama_prd"=>"",
                    "desk_prd"=>"",
                    "sts_jual"=>"",
                    "sts_beli"=>"",
                    "token"=>""
                );

        if($this->val_form_product_user()){
            $id_tipe = $this->input->post("id_tipe");
            $id_user = $this->input->post("id_user");
            $satuan_prd = $this->input->post("satuan_prd");
            $harga_prd = $this->input->post("harga_prd");
            $harga_beli_satuan = $this->input->post("harga_beli_satuan");
            $stok = $this->input->post("stok");
            $stok_opname = "0";
            $nama_prd = $this->input->post("nama_prd");
            $desk_prd = $this->input->post("desk_prd");
            $sts_jual = $this->input->post("sts_jual");
            $sts_beli = $this->input->post("sts_beli");

            $id_prd   = $this->input->post("id_prd");

            $token   = $this->input->post("token");

            $admin_del = 0;
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $set = array(
                        "id_tipe"=>$id_tipe,
                        "satuan_prd"=>$satuan_prd,
                        "harga_prd"=>$harga_prd,
                        "harga_beli_satuan"=>$harga_beli_satuan,
                        "nama_prd"=>$nama_prd,
                        "desk_prd"=>$desk_prd,
                        "sts_jual"=>$sts_jual,
                        "sts_beli"=>$sts_beli
                    );

                $where = array(
                            "id_prd"=>$id_prd
                        );

                $update = $this->mm->update_data("produk", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }else {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                }
            }
        }else{            
            $msg_detail = array(
                            "id_tipe"=>strip_tags(form_error('id_tipe')),
                            "id_user"=>strip_tags(form_error('id_user')),
                            "satuan_prd"=>strip_tags(form_error('satuan_prd')),
                            "harga_prd"=>strip_tags(form_error('harga_prd')),
                            "stok"=>strip_tags(form_error('stok')),
                            "nama_prd"=>strip_tags(form_error('nama_prd')),
                            "desk_prd"=>strip_tags(form_error('desk_prd')),
                            "sts_jual"=>strip_tags(form_error('sts_jual')),
                            "sts_beli"=>strip_tags(form_error('sts_beli')),
                            "token"=>strip_tags(form_error('token'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_product_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete()){
            $id_prd     = $this->input->post("id_prd");
            $token      = $this->input->post("token");
            if($token == "FC094X"){
                if($this->mm->delete_data("produk", array("id_prd"=>$id_prd))){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                }
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


    public function val_get_product_api(){
        $config_val_input = array(
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_product_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_product_tipe_api()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                if($this->val_get_product_api()){
                    $data = $this->pm->get_produk(array("p.id_user"=>$id_user, "p.is_delete"=>"0"));
                    if($data){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        $msg_detail["item"] = $data;
                    }
                }else{
                    $msg_detail["token"] = strip_tags(form_error('token'));
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        // print_r("<pre>");
        // print_r($data);
        print_r(json_encode($data));
    }
#=============================================================================#
#-------------------------------------------product_main----------------------#
#=============================================================================#


}
?>