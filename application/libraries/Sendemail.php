<?php
/**
 * @author Surya Hanggara (Filosofi_code)
 * @copyright 2019
 */
class Sendemail{
    private function server_email(){
        return array(
                    "email_name"=>"suryahanggaraprgs@gmail.com",
                    "profil_name"=>"Surya Hanggara",
                    "title"=>"Command"
                );
    }
    
    private function set_content($paket_link){
        return          "<!DOCTYPE html>
                            <html lang=\"en\">
                            <head>
                                <meta charset=\"utf-8\">
                                <title>Welcome to CodeIgniter</title>
                            
                                <style type=\"text/css\">
                            
                                ::selection { background-color: #E13300; color: white; }
                                ::-moz-selection { background-color: #E13300; color: white; }
                            
                                body {
                                    background-color: #FFF;
                                    margin: 40px;
                                    font: 16px/20px normal Helvetica, Arial, sans-serif;
                                    color: #4F5155;
                                    word-wrap: break-word;
                                }
                            
                                a {
                                    color: #003399;
                                    background-color: transparent;
                                    font-weight: normal;
                                }
                            
                                h1 {
                                    color: #444;
                                    background-color: transparent;
                                    border-bottom: 1px solid #D0D0D0;
                                    font-size: 24px;
                                    font-weight: normal;
                                    margin: 0 0 14px 0;
                                    padding: 14px 15px 10px 15px;
                                }
                            
                                code {
                                    font-family: Consolas, Monaco, Courier New, Courier, monospace;
                                    font-size: 16px;
                                    background-color: #f9f9f9;
                                    border: 1px solid #D0D0D0;
                                    color: #002166;
                                    display: block;
                                    margin: 14px 0 14px 0;
                                    padding: 12px 10px 12px 10px;
                                }
                            
                                #body {
                                    margin: 0 15px 0 15px;
                                }
                            
                                p.footer {
                                    text-align: right;
                                    font-size: 16px;
                                    border-top: 1px solid #D0D0D0;
                                    line-height: 32px;
                                    padding: 0 10px 0 10px;
                                    margin: 20px 0 0 0;
                                }
                            
                                #container {
                                    margin: 10px;
                                    border: 1px solid #D0D0D0;
                                    box-shadow: 0 0 8px #D0D0D0;
                                }
                                </style>
                            </head>
                            <body>
                            
                            <div id=\"container\">
                                <h1>VERIFIKASI AKUN, SIKECIL</h1>
                            
                                <div id=\"body\">
                                    <p>Sistem Informasi</p>
                                    <hr/><br /><br />
                                    
                                    <p>Selamat akun anda telah terdaftar di sistem infromasi kami.</p>
                                    <p>Sebagai tahap akhir dari proses pendaftaran, silahkan saudara klik tombol Vertifikasi, untuk aktifkan akun saudara</p>
                                    <br /><br />
                                    <p>Mohon tekan tombol berikut ini :</p>
                                    
                                    <a href=\"".$paket_link."\">Vertifikasi</a>
                                </div>
                            </div>
                            <br /><br />
                            <br /><br />
                            <br /><br />
                            
                            </body>
                        </html>";
    }
    
    public function send_email_vert($email, $paket_link, $subject){
        
        $email_condition = $this->server_email();
    
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = $email_condition["email_name"]; 
        $config['smtp_pass'] = "indahtri2";
        $config['charset'] = "iso-8859-1";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $ci->email->initialize($config);
        
        $ci->email->from($email_condition["email_name"], $email_condition["profil_name"]);
        $list = $email;
        $ci->email->to($list);
        $ci->email->subject($subject);
        
        //content
        $ci->email->message($this->set_content($paket_link));
        $ci->email->send();
        // print_r($ci->email->print_debugger());
        // echo "sip";
    }





    public function send_email_forget_password($email, $username, $new_password, $subject){
        
        $email_condition = $this->server_email();
    
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = $email_condition["email_name"]; 
        $config['smtp_pass'] = "indahtri2";
        $config['charset'] = "iso-8859-1";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $ci->email->initialize($config);
        
        $ci->email->from($email_condition["email_name"], $email_condition["profil_name"]);
        $list = $email;
        $ci->email->to($list);
        $ci->email->subject($subject);
        
        //content
        $ci->email->message($this->set_content_forget_password($username, $new_password));
        $ci->email->send();
        // print_r($ci->email->print_debugger());
        // echo "sip";
    }

    private function set_content_forget_password($username, $new_password){
        return          "<!DOCTYPE html>
                            <html lang=\"en\">
                            <head>
                                <meta charset=\"utf-8\">
                                <title>Welcome to CodeIgniter</title>
                            
                                <style type=\"text/css\">
                            
                                ::selection { background-color: #E13300; color: white; }
                                ::-moz-selection { background-color: #E13300; color: white; }
                            
                                body {
                                    background-color: #FFF;
                                    margin: 40px;
                                    font: 16px/20px normal Helvetica, Arial, sans-serif;
                                    color: #4F5155;
                                    word-wrap: break-word;
                                }
                            
                                a {
                                    color: #003399;
                                    background-color: transparent;
                                    font-weight: normal;
                                }
                            
                                h1 {
                                    color: #444;
                                    background-color: transparent;
                                    border-bottom: 1px solid #D0D0D0;
                                    font-size: 24px;
                                    font-weight: normal;
                                    margin: 0 0 14px 0;
                                    padding: 14px 15px 10px 15px;
                                }
                            
                                code {
                                    font-family: Consolas, Monaco, Courier New, Courier, monospace;
                                    font-size: 16px;
                                    background-color: #f9f9f9;
                                    border: 1px solid #D0D0D0;
                                    color: #002166;
                                    display: block;
                                    margin: 14px 0 14px 0;
                                    padding: 12px 10px 12px 10px;
                                }
                            
                                #body {
                                    margin: 0 15px 0 15px;
                                }
                            
                                p.footer {
                                    text-align: right;
                                    font-size: 16px;
                                    border-top: 1px solid #D0D0D0;
                                    line-height: 32px;
                                    padding: 0 10px 0 10px;
                                    margin: 20px 0 0 0;
                                }
                            
                                #container {
                                    margin: 10px;
                                    border: 1px solid #D0D0D0;
                                    box-shadow: 0 0 8px #D0D0D0;
                                }
                                </style>
                            </head>
                            <body>
                            
                            <div id=\"container\">
                                <h1>SIKECIL, INFORMASI KEANGGOTAAN</h1>
                            
                                <div id=\"body\">
                                    <p>Sistem Informasi</p>
                                    <hr/><br /><br />
                                    
                                    <p>Dengan anda menggunakan menu lupa password maka password anda akan di ubah pada posisi acak, berikut adalah info mengenai username dan password anda: </p>
                                    <br /><hr><br />
                                    <p>username : ".$username."</p>
                                    <p>password : ".$new_password."</p>
                                    <br /><hr><br />
                                    <p>untuk mempermudah langkah anda selanjutnya silahkan ubah password anda sesuai yang anda kehendaki</p>
                                </div>
                            </div>
                            <br /><br />
                            <br /><br />
                            <br /><br />
                            
                            </body>
                        </html>";
    }
    

    private function set_content_vert_ch_pass($paket_link){
        return          "<!DOCTYPE html>
                            <html lang=\"en\">
                            <head>
                                <meta charset=\"utf-8\">
                                <title>Welcome to CodeIgniter</title>
                            
                                <style type=\"text/css\">
                            
                                ::selection { background-color: #E13300; color: white; }
                                ::-moz-selection { background-color: #E13300; color: white; }
                            
                                body {
                                    background-color: #FFF;
                                    margin: 40px;
                                    font: 16px/20px normal Helvetica, Arial, sans-serif;
                                    color: #4F5155;
                                    word-wrap: break-word;
                                }
                            
                                a {
                                    color: #003399;
                                    background-color: transparent;
                                    font-weight: normal;
                                }
                            
                                h1 {
                                    color: #444;
                                    background-color: transparent;
                                    border-bottom: 1px solid #D0D0D0;
                                    font-size: 24px;
                                    font-weight: normal;
                                    margin: 0 0 14px 0;
                                    padding: 14px 15px 10px 15px;
                                }
                            
                                code {
                                    font-family: Consolas, Monaco, Courier New, Courier, monospace;
                                    font-size: 16px;
                                    background-color: #f9f9f9;
                                    border: 1px solid #D0D0D0;
                                    color: #002166;
                                    display: block;
                                    margin: 14px 0 14px 0;
                                    padding: 12px 10px 12px 10px;
                                }
                            
                                #body {
                                    margin: 0 15px 0 15px;
                                }
                            
                                p.footer {
                                    text-align: right;
                                    font-size: 16px;
                                    border-top: 1px solid #D0D0D0;
                                    line-height: 32px;
                                    padding: 0 10px 0 10px;
                                    margin: 20px 0 0 0;
                                }
                            
                                #container {
                                    margin: 10px;
                                    border: 1px solid #D0D0D0;
                                    box-shadow: 0 0 8px #D0D0D0;
                                }
                                </style>
                            </head>
                            <body>
                            
                            <div id=\"container\">
                                <h1>VERIFIKASI PERUBAHAN PASSWORD AKUN, SIKECIL</h1>
                            
                                <div id=\"body\">
                                    <p>Sistem Informasi</p>
                                    <hr/><br /><br />
                                    
                                    <p>Sebagai tahap lanjut dari pengingat password akun, Kami tealh mengirimkan link berikut, sebagai solusi anda untuk mengakses akun anda.</p>
                                    <p>Silahkan akses link berikut untuk mendapatkan password baru.</p>
                                    <br /><br />
                                    <p>Mohon tekan tombol berikut ini :</p>
                                    
                                    <a href=\"".$paket_link."\">Ubah Password</a>
                                </div>
                            </div>
                            <br /><br />
                            <br /><br />
                            <br /><br />
                            
                            </body>
                        </html>";
    }
    
    public function send_email_vert_ch_pass($email, $paket_link, $subject){
        
        $email_condition = $this->server_email();
    
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = $email_condition["email_name"]; 
        $config['smtp_pass'] = "indahtri2";
        $config['charset'] = "iso-8859-1";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $ci->email->initialize($config);
        
        $ci->email->from($email_condition["email_name"], $email_condition["profil_name"]);
        $list = $email;
        $ci->email->to($list);
        $ci->email->subject($subject);
        
        //content
        $ci->email->message($this->set_content_vert_ch_pass($paket_link));
        $ci->email->send();
        // print_r($ci->email->print_debugger());
        // echo "sip";
    }


    
}

?>
